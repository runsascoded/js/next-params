import { NextRouter, useRouter } from "next/router";
import { Dispatch, useCallback, useEffect, useMemo, useState } from "react";
import { entries, fromEntries, keys, mapEntries, mapValues, values } from "@rdub/base/objs";
import _ from "lodash";
import { Param, Params, ParamStates } from "./params";

export const getHash = () => (typeof window !== 'undefined' ? window.location.hash.replace('#', '') : undefined);

export type HashMapVal<T> = { val: T, param: Param<T> }
export type HashMap<T> = { [k: string]: HashMapVal<T> }
export function getHashMap<
    Params extends { [k: string]: Param },
    Out extends HashMap<any>
>(
    params: Params,
    hash?: string,
): Out {
    hash = hash || getHash()
    if (hash && hash.startsWith('#')) {
        hash = hash.substring(1)
    }
    const hashPieces = hash ? hash.split('&') : []
    // console.log("hashPieces:", hashPieces)
    const hashMap = {} as HashMap<any>
    hashPieces.forEach(piece => {
        const [ k, vStr] = piece.split(/=(.*)/)
        const param = params[k]
        if (!param) {
            throw new Error(`Unrecognized param key: ${k}, keys ${keys(params)}, vStr ${vStr}`)
        }
        const val = param.decode(vStr)
        // console.log("decoded:", k, vStr, val)
        hashMap[k] = { val, param };
    })
    // console.log("hashMap:", hashMap)
    return hashMap as Out
}

export function updatedHash<Params extends { [k: string]: Param }>(
    params: Params,
    newVals: { [k: string]: any },
) {
    const hashMap = getHashMap(params)
    entries(newVals).forEach(([ k, val ]) => {
        const param = params[k]
        if (!param) {
            throw Error(`Unrecognized param key: ${k}`)
        }
        hashMap[k] = { val, param }
    })
    const newHash = entries(hashMap).map(([ k, { val, param, } ]) => {
        const valStr = param.encode(val)
        if (valStr === undefined) return undefined
        return `${k}=${valStr}`
    }).filter(s=> s).join('&')
    // console.log("updatedHash:", newHash, hashMap)
    return newHash
}

export function getHistoryStateHash() {
    // Assume next.js has written a `history.state` that includes `{ url: string, as: string }`
    const url = history.state.url as string
    let hash = url.replace(/^[^#]*#?/, '')
    if (!hash) {
        const as = history.state.as as string
        hash = as.replace(/^[^#]*#?/, '')
        if (hash) {
            console.warn(`no hash in history state url ${url}, using as ${as}`)
        } else {
            // console.warn(`no hash in history state url ${url} or as ${as}`)
        }
    }
    return hash
}

export function updateHashParams<Params extends { [k: string]: Param }>(
    params: Params,
    newVals: { [k: string]: any },
    { push, log }: { push?: boolean, log?: boolean },
): string {
    const hash = updatedHash(params, newVals)
    const hashStr = `#${hash}`
    if (window.location.hash == hashStr) {
        if (log) console.log("updateHashParams: skipping push", window.location.hash, "→", hashStr, window.location.hash == hashStr)
    } else if (push) {
        if (log) console.log("updateHashParams: push (pushState)", window.location.hash, "→", hashStr, window.location.hash == hashStr)
        window.history.pushState({ url: hashStr }, '', hashStr)
    } else {
        if (log) console.log("updateHashParams: push (replaceState)", window.location.hash, "→", hashStr, window.location.hash == hashStr)
        window.history.replaceState({ url: hashStr }, '', hashStr)
    }
    return hash
}

export function updateHashParamsRouter<Params extends { [k: string]: Param }>(
    params: Params,
    newVals: { [k: string]: any },
    router: NextRouter,
    { push, log }: { push?: boolean, log?: boolean },
): Promise<boolean> {
    const hash = updatedHash(params, newVals)
    const hashStr = `#${hash}`
    const url = { hash }
    if (window.location.hash == hashStr) {
        if (log) console.log("updateHashParamsRouter: skipping push", window.location.hash, "→", hashStr, window.location.hash == hashStr)
        return Promise.resolve(false)
    } else if (push) {
        if (log) console.log("updateHashParamsRouter: push (pushState)", window.location.hash, "→", hashStr, window.location.hash == hashStr)
        return router.push(url, url, { shallow: true })
    } else {
        if (log) console.log("updateHashParamsRouter: push (replaceState)", window.location.hash, "→", hashStr, window.location.hash == hashStr)
        return router.replace(url, url, { shallow: true })
    }
}
export function parseHashParams<P extends Params>(
    { params, stateCb, popStateCb }: {
        params: P
        stateCb?: (state: { [k: string]: any }) => void
        popStateCb?: (state: HashMap<any>) => void
    }
): ParamStates<P> {
    // console.log("parseHashParams:", useState)
    const [ initialHash, setInitialHash ] = useState(() => {
        const hash = getHash()
        // console.log("initial hash:", hash)
        return hash
    });

    const initialHashMap = useMemo(() => getHashMap(params, initialHash), [ initialHash, ])

    const state = mapEntries(
        params,
        (k, param) => {
            // const init = param.decode(undefined)
            const [ val, set ] = (param.use || useState)(() => {
                const init = (k in initialHashMap) ? initialHashMap[k].val : param.decode(undefined)
                // console.log(`param ${k} init:`, init)
                return init
            })
            return [ k, { val, set, param } ]
        }
    )
    const stateVals = values(state).map(({ val }) => val)
    // console.log("stateVals:", stateVals)

    const setStates = useCallback(
        (hash?: string) => {
            const hashMap = getHashMap(params, hash)
            entries(hashMap).forEach(([ k, { val } ]) => {
                const { val: cur, set } = state[k]
                const eq = _.isEqual(cur, val)
                // console.log(`param ${k}, eq?`, eq, cur, val)
                if (!eq) {
                    console.log(`update state: ${k}`, cur, "->", val, `(change: ${!eq})`)
                    set(val)
                }
            })
        },
        [ params, state, ]
    )

    // Configure browser "back" button
    useEffect(
        () => {
            const fn = (e: PopStateEvent) => {
                console.log("onpopstate", e.state, `"${(e.target as Window).location.hash}"`, `"${window.location.hash}"`, e)
                console.log("history.state:", history.state, `"${history.state.hash}"`)
                const hash = getHistoryStateHash()
                setStates(hash)
                if (popStateCb) {
                    const hashMap = getHashMap(params, hash)
                    popStateCb(hashMap)
                }
            }
            window.addEventListener('popstate', fn);
            return () => {
                window.removeEventListener('popstate', fn);
            }
        },
        [ setStates, popStateCb ]
    )

    useEffect(() => {
        const handleHashChange = () => {
            console.log("handleHashChange")
            setStates()
        };
        window.addEventListener('hashchange', handleHashChange);
        // setStates(initialHash)
        return () => {
            window.removeEventListener('hashchange', handleHashChange);
        };
    }, []);

    // State -> URL query values
    useEffect(
        () => {
            if (stateCb) {
                stateCb(state)
            }
        },
        stateVals,
    )

    return mapValues(state, (k, { val, set, }) => [ val, set, ]) as ParamStates<P>
}

export function parseHash<Params extends Record<string, Param>>(hash: string, params: Params) {
    return hash
        .split('&')
        .map(kv => {
            if (!kv) return
            const [ k, v ] = kv.split('=', 2)
            if (!(k in params)) {
                console.warn("unknown param:", k)
                return
            }
            const param = params[k]
            const val = param.decode(v)
            return [ k, val ]
        })
        .filter((a): a is [ string, any ] => !!a)
}

export function useHashParams<
    P extends Params,
    State extends ParamStates<P>,
>(
    {
        params,
        log: _log = false,
    }: {
        params: P
        log?: boolean
    }
): { state: State | undefined, updateState: Dispatch<Partial<State>>, } {
    const log = useCallback(
        (...args: any[]) => {
            if (_log) {
                console.log(...args)
            }
        },
        [ _log, ]
    )
    const defaultState = useMemo(() => mapValues(params, (k, v) => v.decode(undefined)) as State, [ params ])
    const [ state, setState ] = useState<State | undefined>(() => {
        if (typeof window === 'undefined') return
        let initHash = window.location.hash
        if (initHash.startsWith("#")) {
            initHash = initHash.substring(1)
        }
        const parsedHash = fromEntries(parseHash(initHash, params))
        const initState = mapValues(params, (k, v) => {
            if (k in parsedHash) {
                return parsedHash[k]
            } else {
                return v.decode(undefined)
            }
        }) as State
        log("hash state: initState", initState)
        return initState
    })
    const router = useRouter()
    const hash = useMemo(
        () => {
            if (!router.isReady) {
                log("hash state: router not ready")
                return undefined
            }
            const pieces = router.asPath.split("#", 2)
            if (pieces.length < 2) {
                log("hash state: empty hash")
                return ""
            }
            return pieces[1]
        },
        [ router.isReady, router.asPath, log ]
    )
    useEffect(() => {
        if (hash === undefined) {
            log("hash state: hash undefined")
            return
        }
        if (state === undefined) {
            log("hash state: state undefined")
            return
        }
        // const hash = router.asPath.split("#", 2)[1]
        log("hash state, hash:", hash, "cur state:", state)
        let hasNew = false
        let newState: State = { ...defaultState }
        const hashState = fromEntries(parseHash(hash, params)) as State
        for (const [ k, cur ] of entries(state)) {
            // const param = params[k]
            if (k in hashState) {
                newState[k] = hashState[k]
            }
            // const val = param.decode(v)
            if (!_.eq(cur, newState[k])) {
                hasNew = true
            }
        }
        if (hasNew) {
            log("hash state: newState from hash", newState)
            setState(newState as State)
        } else {
            log("hash state: no new state:", newState)
        }
    }, [ hash, state, setState, log, ])

    const updateState = useCallback(
        (newState: Partial<State>) => {
            if (!state) {
                console.warn("hash state: updateState: no state")
                return
            }
            const mergedState = { ...state, ...newState }
            log("hash state: updateState:", mergedState)
            setState(mergedState)
        },
        [ state, log, ]
    )

    return { state, updateState, }
}
