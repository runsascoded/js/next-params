import { Dispatch } from "react";
import _ from "lodash";
import { Actions, OptActions, useOptSet, useSet } from "@rdub/base/use-set";

export const pathnameRegex = /[^?#]+/u;
export const pathQueryRegex = /[^#]+/u;

export type Param<T = any, U = Dispatch<T>> = {
    encode: (t: T) => string | undefined
    decode: (v: string | undefined) => T
    push?: boolean
    use?: (init: T) => [ T, U ]
}

export type Params = Record<string, Param>

export type ParamType<P> = P extends Param<infer T> ? T : never

// Helper type to convert an object of Params to an object of their value types
export type ParsedParams<P extends Params> = {
    [K in keyof P]: ParamType<P[K]>
}

export type ParamState<T> = [ T, Dispatch<T> ]

export type ParamStateType<P> = P extends Param<infer T> ? ParamState<T> : never

export type ParamStates<P extends Params> = {
    [K in keyof P]: ParamStateType<P[K]>
}

export function stringParam(push: boolean = true): Param<string | undefined> {
    return {
        encode: v => v,
        decode: v => v,
        push,
    }
}

export function hashStringParam(push: boolean = true): Param<string | undefined> {
    return {
        encode: v => v ? encodeURI(v.replaceAll('+', '%2B').replaceAll(' ', '+')) : v,
        decode: v => v ? decodeURI(v).replaceAll('+', ' ') : v,
        push,
    }
}

export function defStringParam(init: string, push: boolean = true): Param<string> {
    return {
        encode: v => v == init ? undefined : v,
        decode: v => v == undefined ? init : v,
        push,
    }
}

/**
 * Param for storing URLs specifically; strips off the leading "https://"
 * @param init initial/default value, query param is omitted iff the value matches this
 * @param push whether to push changes into the browser's history/navigation stack
 */
export function urlParam(init: string, push: boolean = true): Param<string> {
    return {
        encode: v => {
            if (v == init) return undefined
            return v.replace(/^https:\/\//, '')
        },
        decode: v => {
            if (v === undefined) return init
            return v.startsWith('http') ? v : `https://${v}`
        },
        push,
    }
}

export function intParam(init: number, push: boolean = true): Param<number> {
    return {
        encode: v => v === init ? undefined : v.toString(),
        decode: v => v ? parseInt(v) : init,
        push,
    }
}

export function optIntParam(push: boolean = true): Param<number | null> {
    return {
        encode: v => {
            if (v === null) return undefined
            return v.toString()
        },
        decode: v => {
            if (v === undefined) return null
            return parseInt(v)
        },
        push,
    }
}

export function floatParam(init: number, push: boolean = true): Param<number> {
    return {
        encode: v => v === init ? undefined : v.toString(),
        decode: v => v ? parseFloat(v) : init,
        push,
    }
}

const { entries, fromEntries, keys, } = Object

export function stringsParam(props?: { init?: string[], delimiter?: string }): Param<string[], Actions<string>> {
    const { init = [], delimiter = ' ' } = props || {}
    const encodedInit = init.join(delimiter)
    return {
        encode: values => {
            const enc = values.join(delimiter)
            if (enc === encodedInit) return undefined
            return enc
        },
        decode: s => {
            if (!s && s !== '') {
                return init
            }
            return s.split(delimiter)
        },
        use: useSet,
    }
}

export function optStringsParam(props?: { emptyIsUndefined?: boolean, delimiter?: string }): Param<null | string[], OptActions<string>> {
    const { emptyIsUndefined = false, delimiter = ' ' } = props || {}
    return {
        encode: (values: string[] | null) => {
            if (values === null) {
                return emptyIsUndefined ? '' : undefined
            } else if (values.length == 0) {
                return emptyIsUndefined ? undefined : ''
            } else {
                return values.join(delimiter)
            }
        },
        decode: (s: string | undefined): string[] | null => {
            if (s === undefined) {
                return emptyIsUndefined ? [] : null
            } else if (s === '') {
                return emptyIsUndefined ? null : []
            } else {
                return s.split(delimiter)
            }
        },
        use: useOptSet,
    }
}

export function enumMultiParam<T extends string>(
    init: T[],
    mapper: { [k in T]: string } | [ T, string ][],
    delim?: string,
): Param<T[]> {
    const delimiter: string = delim === undefined ? '_' : delim
    const t2s: { [k in T]: string } = (mapper instanceof Array) ? fromEntries(mapper) as { [k in T]: string } : mapper
    const s2t: { [key: string]: T } = fromEntries(entries(t2s).map(([ k, v, ]) => [ v, k, ]))

    function verify(values: string[]): T[] {
        return Array.from(values).filter(
            (v): v is T => {
                if (v in t2s) {
                    return true
                } else {
                    console.warn(`Invalid value: ${v} not in ${keys(t2s).join(", ")}`)
                    return false
                }
            }
        )
    }

    const encode = (values: T[]) => {
        return verify(values).map(v => t2s[v]).join(delimiter)
    }

    const encodedInit = encode(init)

    return {
        encode: values => {
            const enc = encode(values)
            if (enc === encodedInit) return undefined
            return enc
        },
        decode: s => {
            if (!s && s !== '') {
                return init
            }
            let values = s.split(delimiter).filter(v => {
                if (v in s2t) {
                    return true
                } else {
                    console.warn(`Unrecognized value: ${v} not in ${keys(s2t).join(",")}`)
                    return false
                }
            }).map(v => s2t[v])
            values = verify(values)
            return values
        },
    }
}

export function enumParam<T extends string>(
    init: T,
    mapper: { [k in T]: string } | [ T, string ][]
): Param<T> {
    const t2s: { [k in T]: string } = (mapper instanceof Array) ? fromEntries(mapper) as { [k in T]: string } : mapper
    const s2t: { [key: string]: T } = fromEntries(entries(t2s).map(([ k, v, ]) => [ v, k, ]))
    return {
        encode(t: T): string | undefined {
            if (t == init) return undefined
            return t2s[t];
        },
        decode(v: string | undefined): T {
            if (v === undefined) return init
            if (!(v in s2t)) {
                console.warn(`Invalid enum: ${v} not in ${keys(s2t).join(",")}`)
                return init
            }
            return s2t[v]
        },
    }
}

export const boolParam: Param<boolean> = {
    encode(t: boolean): string | undefined {
        return t ? '' : undefined;
    },
    decode(v: string | undefined): boolean {
        return v !== undefined;
    },
}

export function numberArrayParam(
    defaultValue: number[] = [],
): Param<number[]> {
    const eq = _.isEqual
    return {
        encode(value: number[]): string | undefined {
            if (eq(value, defaultValue)) return undefined
            return value.map(v => v.toString()).join(',')
        },
        decode(value: string | undefined): number[] {
            if (value === undefined) return defaultValue
            if (value === '') return []
            return value.split(',').map(parseInt)
        },
    }
}

export function parseLLs(str: string): number[] {
    const matches = Array.from(str.matchAll(/[ +\-_]/g))
    const lls = [] as number[]
    let prvMatch: RegExpMatchArray | null = null
    let startIdx = 0
    function parseLL(endIdx: number) {
        startIdx = prvMatch ? prvMatch.index || str.length : 0
        let sep = prvMatch ? prvMatch[0] : ''
        if (sep != '-') {
            startIdx += sep.length
        }
        let piece = str.substring(startIdx, endIdx)
        startIdx = endIdx
        if (!piece) return
        const float = parseFloat(piece)
        if (isNaN(float)) {
            throw new Error(`Invalid piece ${piece}, parsing ${str}`)
        }
        lls.push(float)
    }
    matches.forEach((match, idx) => {
        let endIdx = match.index || str.length
        parseLL(endIdx)
        prvMatch = match
    })
    if (startIdx < str.length) {
        parseLL(str.length)
    }
    // console.log("parsed LLs:", str, lls)
    return lls
}

export type LL = { lat: number, lng: number }
export type LLParam = { init: LL, places?: number, push?: boolean }
export function llParam({ init, places, push }: LLParam): Param<LL> {
    return {
        encode: ({ lat, lng }) => {
            if (lat === init.lat && lng === init.lng) return undefined
            const [ l, L ] = places ? [ lat.toFixed(places), lng.toFixed(places) ] : [ lat, lng ]
            return (lng < 0) ? `${l}${L}` : `${l}_${L}`
        },
        decode: v => {
            if (!v) return init
            const lls = parseLLs(v)
            if (lls.length != 2) {
                console.warn(`Unrecognized ll value: ${v}`)
            }
            const [ lat, lng ] = lls
            return { lat, lng }
        },
        push,
    }
}

export type BB = { sw: LL, ne: LL }
export type BBParam = { init: BB, places?: number, push?: boolean }
export function bbParam({ init, places, push }: BBParam): Param<BB> {
    return {
        encode: ({ sw, ne }) => {
            if (sw.lat === init.sw.lat && sw.lng === init.sw.lng && ne.lat == init.ne.lat && ne.lng == init.ne.lng) return undefined
            const sws = places ? [ sw.lat.toFixed(places), sw.lng.toFixed(places) ] : [ sw.lat, sw.lng ]
            const nes = places ? [ ne.lat.toFixed(places), ne.lng.toFixed(places) ] : [ ne.lat, ne.lng ]
            const lls = [ ...sws, ...nes ]
            let str = ''
            lls.forEach((ll, idx) => {
                if (idx > 0) {
                    str += idx > 0 ? ' ' : ''
                }
                str += ll.toString()
            })
            // console.log("encoded", sw, ne, str)
            return str
        },
        decode: v => {
            if (!v) return init
            const lls = parseLLs(v)
            if (lls.length != 4) {
                console.warn(`Unrecognized ll value: ${v}, ${JSON.stringify(lls)}`)
            }
            const [ swLat, swLng, neLat, neLng ] = lls
            return { sw: { lat: swLat, lng: swLng }, ne: { lat: neLat, lng: neLng } }
        },
        push,
    }
}
