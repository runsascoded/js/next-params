import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { entries, fromEntries, mapEntries, mapValues, o2a } from "@rdub/base/objs";
import _ from "lodash";
import { Param, Params, ParamState, ParamStates, ParsedParams, pathnameRegex } from "./params";
import { NextApiRequest } from "next";

export function decode<P extends Params>(
  req: NextApiRequest,
  params: P,
): ParsedParams<P> {
  return mapEntries(params, (k, param) => {
    let qv = req.query[k]
    if (qv instanceof Array) {
      const n = qv.length
      if (n > 1) {
        console.warn(`Multiple values for ${k}: ${qv}; using last`)
      }
      qv = qv[n - 1]
    }
    return [k, param.decode(qv)]
  }) as ParsedParams<P>
}

export function encode<T extends Record<string, Param>>(
  params: T,
  vals: ParsedParams<T>,
): string {
  return new URLSearchParams(
    entries(params).map(([ k, param ]) => {
      const v = param.encode(vals[k])
      return v === undefined ? undefined : [k, v]
    }).filter((x): x is [string, string] => x !== undefined)
  ).toString()
}

export function parseQueryParams<P extends Params>({ params }: { params: P }): ParamStates<P> {
    const router = useRouter()
    const { isReady, query } = router
    const [ initialized, setInitialized ] = useState(false)
    const path = router.asPath
    const pathSearchStr = path.replace(pathnameRegex, '')
    const pathSearchParams = new URLSearchParams(pathSearchStr)
    const pathQuery = fromEntries(pathSearchParams.entries()) as { [k: string]: string }
    const [ initialQuery, setInitialQuery ] = useState(pathQuery)
    const state = mapEntries(
        params,
        (k, param) => {
            // Using the query string value (`pathQuery[k]`) initializes useState with a value that matches what it's
            // supposed to be (given the URL query params), but it can trigger
            // https://legacy.reactjs.org/docs/error-decoder.html/?invariant=418 ("Hydration failed because the initial
            // UI does not match what was rendered on the server", because the server is generally statically rendered
            // with no query params.
            //
            // We pass `undefined` instead, which lets hydration proceed as normal, but then once `router.isReady`, we
            // go through all the params and set them to the page's initial query-param values. That seems to work in
            // all cases.
            const init = param.decode(undefined)
            const [ val, set ] = (param.use || useState)(init)
            // console.log(`param ${k} init:`, val, set)
            return [ k, { val, set, param } ]
        }
    )

    // console.log(`parseQueryParams init: query:`, query, "pathQuery:", pathQuery, "state vals:", mapEntries(state, (k, { val }) => [ k, val ]))

    // Configure browser "back" button
    useEffect(
        () => {
            window.onpopstate = e => {
                const newUrl = e.state.url
                const newSearchStr = newUrl.replace(pathnameRegex, '')
                // console.log("onpopstate:", e, "newUrl:", newUrl, "newSearchStr:", newSearchStr, "oldSearchStr:", pathSearchStr)
                const newSearchObj = fromEntries(new URLSearchParams(newSearchStr).entries())
                Object.entries(params).forEach(([ k, param ]) => {
                    const val = param.decode(newSearchObj[k])
                    const { val: cur, set } = state[k]
                    const eq = _.isEqual(cur, val)
                    // console.log(`param ${k} eq? (back)`, eq, cur, val)
                    if (!eq) {
                        // console.log(`back! setting: ${k}, ${cur} -> ${val} (change: ${!eq})`)
                        set(val)
                    }
                })
            };
        },
        [ path, ]
    );

    // Initial URL query -> state values, once `router.isReady`. This sets `initialized`, which allows subsequent
    // effects to run.
    useEffect(
        () => {
            if (!isReady) return
            // console.log("Setting state to initial query values:", initialQuery)
            entries(initialQuery).forEach(([ k, str ]) => {
                const param = params[k]
                if (!param) {
                    console.warn(`Unrecognized param: ${k}=${str}`)
                    return
                }
                const init = initialQuery[k]
                const newVal = param.decode(init)
                const { val, set } = state[k]
                const eq = _.isEqual(val, newVal)
                // console.log(`param-${k} eq? (init from URL)`, eq, val, newVal)
                if (!eq) {
                    // console.log(`${k}: setting initial query state:`, val, newVal)
                    set(newVal)
                }
            })
            setInitialized(true)
        },
        [ isReady ]
    )

    // URL -> state values
    useEffect(
        () => {
            if (!initialized) {
                console.log("Skipping state initialization, !initialized")
                return
            }
            // console.log("updating states: path", path, ", searchStr:", pathSearchStr)
            Object.entries(params).forEach(([ k, param ]) => {
                const qv = query[k]
                const qval: string | undefined = (qv && qv instanceof Array) ? qv[0] : qv
                const val = param.decode(qval)
                const { val: cur, set } = state[k]
                const eq = _.isEqual(cur, val)
                // console.log(`param-${k} eq? (URL)`, eq, cur, val)
                if (!eq) {
                    // console.log(`update state: ${k}, ${cur} -> ${val} (change: ${!eq})`)
                    set(val)
                }
            })
        },
        [ path, initialized ]
    );

    const match = path.match(pathnameRegex);
    const pathname = match ? match[0] : path;

    // State -> URL query values
    const stateQuery: {[k: string]: string} = {}
    Object.entries(state).forEach(([ k, { val, param, } ]) => {
        const s = param.encode(val)
        // console.log(`param-${k} state->URL:`, val, s)
        if (s !== undefined) {
            stateQuery[k] = s
        }
    })

    const search = o2a(stateQuery, (k, v) => v == '' ? k : `${k}=${encodeURIComponent(v).replace(/%20/g, "+")}`).join("&")
    // console.log(`path: ${path}, searchStr: ${pathSearchStr}, query: `, query, `, search: ${search}, stateQuery:`, stateQuery)

    useEffect(
        () => {
            if (!initialized) {
                console.log("Skipping url update!! !initialized")
                return
            }
            const hash = ''
            const changedKeys = []
            for (const [key, value] of entries(stateQuery)) {
                if (key in params && (!(key in query) || !_.isEqual(value, stateQuery[key]))) {
                    changedKeys.push(key)
                }
            }
            for (const [key, value] of entries(query as Record<string, unknown>)) {
                if (key in params && !changedKeys.includes(key) && (!(key in stateQuery) || !_.isEqual(value, query[key]))) {
                    changedKeys.push(key)
                }
            }
            let push = false
            for (const key of changedKeys) {
                const param = params[key]
                if (param.push) {
                    push = true
                    break
                }
            }
            const url = { pathname: router.pathname, hash, search}
            const as = { pathname, hash, search, }
            const options = { shallow: true, scroll: false, }
            // const method = push ? "push" : "replace"
            console.log(`router.${push ? "push" : "replace"}:`, { pathname: router.pathname, hash, search}, "changedKeys:", changedKeys)
            if (push) {
                router.push(url, as, options)
            } else {
                router.replace(url, as, options)
            }
        },
        [ pathname, router.pathname, search, initialized ]
    )

    return mapValues(state, (k, { val, set, }) => [ val, set, ]) as ParamStates<P>
}

