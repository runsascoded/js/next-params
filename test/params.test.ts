import {describe, expect, test} from '@jest/globals';
import {parseLLs} from '../src/params';

describe('test parseLLs', () => {
    test('basic parsing to work, with or without underscore separator', () => {
        expect(parseLLs("40.732-74.025")).toEqual([40.732, -74.025]);
        expect(parseLLs("40.732_-74.025")).toEqual([40.732, -74.025]);
    });
});
