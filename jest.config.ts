import type {Config} from '@jest/types';
const config: Config.InitialOptions = {
  verbose: true,
  transform: {
    "^.+\\.(ts|js)$": "babel-jest",
  },
  transformIgnorePatterns: [
    "\\.\\./\\.\\./node_modules/(?!@rdub/base)",
  ],
  roots: [ "test" ],
};
export default config;
