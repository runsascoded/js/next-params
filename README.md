# @rdub/next-params
Persist [Next.js] app state in URL parameters

<a href="https://npmjs.org/package/@rdub/next-params" title="View @rdub/next-params on NPM"><img src="https://img.shields.io/npm/v/@rdub/next-params.svg" alt="@rdub/next-params NPM version" /></a>

[Next.js]: https://nextjs.org/
